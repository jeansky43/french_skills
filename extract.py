import pandas as pd
import sqlite3

df = pd.read_excel("Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx", sheet_name="Onglet 3 - référentiel NPEC", skiprows=3)

df.columns=['code_RNCP', 'titre_formation', 'organisme', 'diplome', 'code_CPNE', 'CPNE', 'amount', 'statut', 'date']

df['date'] = pd.to_datetime(df['date']).dt.strftime('%Y-%m-%d')

# print(df)

df_idcc = pd.read_excel("Referentiel-des-NPEC-15.10.2023_vMAJ-29.01.2024.xlsx", sheet_name="Onglet 4 - CPNE-IDCC", skiprows=2)

df_idcc.columns=['code_CPNE', 'CPNE', 'IDCC']

# print(df_idcc)

# Je connecte la base de données
conn = sqlite3.connect('skills.db')

# Je crée le cursor
cur = conn.cursor()

# Création des tables:
cur.execute('''CREATE TABLE IF NOT EXISTS Formation (
                code_RNCP TEXT PRIMARY KEY,
                titre_formation TEXT,
                organisme TEXT,
                diplome TEXT,
                code_CPNE INTEGER,
                CPNE TEXT,
                amount REAL,
                statut TEXT CHECK (statut IN ('A', 'CPNE', 'R')),
                date DATE
            )'''
)
            
cur.execute('''CREATE TABLE IF NOT EXISTS IDCC (
               idcc INTEGER PRIMARY KEY,               
               code_CPNE INTEGER,
               FOREIGN KEY (code_CPNE) REFERENCES Formation (code_CPNE)
            )'''            
)

# Insérer les données dans la table Formation
df[['code_RNCP', 'titre_formation', 'organisme', 'diplome', 'code_CPNE', 'CPNE', 'amount', 'statut', 'date']].to_sql('Formation', conn, if_exists='replace', index=False)

# Insérer les données dans la table IDCC
df_idcc[['IDCC', 'code_CPNE']].to_sql('IDCC', conn, if_exists='replace', index=False)

conn.commit()

conn.close()

